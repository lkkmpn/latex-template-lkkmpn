# `latex-template-lkkmpn`

This is my personal LaTeX template containing all the packages and styles I
use in LaTeX documents.

## Installation

To install in your LaTeX distribution, clone this repo into the `local` folder
of your distribution:

Linux:

```bash
mkdir -p ~/texmf/tex/latex/local
cd ~/texmf/tex/latex/local
git clone git@gitlab.com:lkkmpn/latex-template-lkkmpn.git
```

Windows:

```bash
mkdir [path-to-texlive]\texmf-local\tex\latex\local
cd [path-to-texlive]\texmf-local\tex\latex\local
git clone git@gitlab.com:lkkmpn/latex-template-lkkmpn.git
```

You might need to refresh your file database for your LaTeX compiler to find
the `.sty` file. For TeX Live:

1. Open TeX Live Manager
2. Actions → Regenerate filename database

To use on [Overleaf](https://www.overleaf.com/), click the following link:

[New Overleaf project](https://www.overleaf.com/docs?snip_uri=https://gitlab.com/lkkmpn/latex-template-lkkmpn/-/archive/master/latex-template-lkkmpn-master.zip)

This link will create a new Overleaf project from the contents in this repo.
The [`document.tex`](document.tex) file will be used as main document for this
project.

## Usage

Start your preamble as follows:

```latex
\documentclass[12pt,fleqn]{article}
\usepackage{latex-template-lkkmpn}
% add more packages as necessary

\geometry{a4paper,margin=1in,includehead,includefoot}  % adjust as necessary

\addbibresource{bibliography.bib}  % adjust as necessary

\title{Title}
\subtitle{Subtitle}
\thirdline{Third line}

\begin{document}
…
```

An example document using this template is included in this repo as
[`document.tex`](document.tex).
